
많은 사람들이 주로 [command-line interfaces CLIs)](https://realpython.com/comparing-python-command-line-parsing-libraries-argparse-docopt-click/)를 통해 실행되는 Python 어플리케이션을 사용한다. 여기서부터 빈 캔버스로 시작하는 경우가 많은데, Python 어플리케이션 레이아웃의 유연성은 정말 골칫거리가 될 수 있다.

빈 프로젝트 폴더로 시작하는 것은 위협적일 수 있으며 코더의 블록이 부족하지 않을 수 있다. 이 장에서는 모든 Python CLI 어플리케이션의 시작으로 개인적으로 사용하는 검증된 레이아웃을 공유하고자 한다.

먼저 매우 기본적인 사례에 대한 기본적인 레이아웃, 즉 자체적으로 실행되는 간단한 스크립트부터 살펴보자. 그런 다음 사례가 진행됨에 따라 레이아웃을 구성하는 방법을 볼 수 있도록 진행한다.

## 일회상 스크립트
`.py` 스크립트만 만들면 돼, 그게 핵심이지? 설치할 필요 없이 디렉터리에서 스크립트를 실행한다.

개발자는 단지 자신의 사용을 위한 스크립트를 만들거나 외부 [종속성](https://realpython.com/courses/managing-python-dependencies/)이 없는 스크립트를 만들거면 좋다. 하지만 그것을 배포해야 한다면 어떨까? 특히 Python에 익숙하지 않은 일반 사용자에게?

다음 레이아웃은 이러한 모든 경우에 적용할 수 있으며 워크플로우를 사용하는 설치 또는 기타 도구에 반영하도록 쉽게 수정할 수 있다. 이 레이아웃은 순수 Python 스크립트(즉, 종속성이 없는 스크립트)를 만들거나 [pip](https://pypi.org/project/pip/) 또는 [Pipenv](https://sdldocs.gitlab.io/pythonnecosystem/pipenv-guide/)와 같은 도구를 사용하더라도 적용할 수 있다.

이 페이지를 읽는 동안 레이아웃에서 파일의 정확한 위치는 파일이 그 위치에 배치되는 이유보다는 중요하지 않다는 점에 유의하여야 한다. 모든 파일은 프로젝트 이름을 딴 프로젝트 디렉터리에 있어야 한다. 이 예에서는 프로젝트 이름 및 루트 디렉터리로 `hello world`를 사용한다. 

CLI 앱을 위한 사용하는 일반적인 Python 프로젝트 구조는 다음과 같다.

```
helloworld/
│
├── .gitignore
├── helloworld.py
├── LICENSE
├── README.md
├── requirements.txt
├── setup.py
└── tests.py
```

이는 매우 명백하다. 모든 것이 같은 디렉토리에 있다. 여기에 모든 파일을 나탄낸 것은 아니지만, 이와 같은 기본 레이아웃을 사용할 계획이라면 파일 수를 최소로 유지하는 것이 바람직하다. 이러한 파일 중 일부는 낯설 것이므로 각 파일의 기능에 대해 간단히 살펴보자.

- `.gitignore`: 이것은 Git에게 IDE clutter 또는 로컬 구성 파일과 같이 버전 관리에서 제외할 파일 종류를 명시하는 파일이다. [Git 튜토리얼](https://sdldocs.gitlab.io/gitngitflow/)에 모든 세부 정보가 수록되어 있으며, [여기](https://github.com/github/gitignore)에서 Python 프로젝트에 대한 샘플 .gitignore 파일을 볼 수 있다.

- `helloworld.py`: 이 파일은 여러분이 개발하여 배포하려는 스크립트이다. 기본 스크립트 파일 이름 명명은 프로젝트 이름(최상위 디렉터리 이름과 동일)을 사용하는 것이 바람직하다.

- `LICENSE`: 이 텍스트 파일은 프로젝트에 사용 중인 라이센스를 설명한다. 코드를 배포하는 경우에는 항상 이 파일이 있는 것이 바람직하다. 관례상 파일명은 모두 대문자로 되어 있다.

> **Note**: 프로젝트의 라이센스를 선택하는 데 도움이 필요하다면, [ChooseAlicense](https://choosealicense.com/) 페이지를 참고하시오.

- `README.md`: 어플리케이션의 목적과 사용을 문서화하는 [Markdown](https://en.wikipedia.org/wiki/Markdown)(또는 [reStructuredText](https://en.wikipedia.org/wiki/ReStructuredText)) 파일이다. 훌륭한 README을 만드는 것도 하나의 기술이지만, [여기서](https://dbader.org/blog/write-a-great-readme-for-your-github-project) 숙달하는 방법을 익힐 수 있다.

- `requirements.txt`: 이 파일은 어플리케이션에 대한 Python 종속성과 해당 버전을 명세한다.

- `setup.py`: 이 파일은 종속성을 정의하는 데도 사용할 수 있지만 설치 중에 수행해야 하는 다른 작업을 기술하는 데 매우 적합합니다. [Pipenv: 새 Python 패키징 도구](https://sdldocs.gitlab.io/pythonnecosystem/pipenv-guide/)에서 `setup.py`와  `requirements.txt`에 대해 자세히 알아볼 수 있다.

- `tests.py`: 이 스크립트에는 테스트가 포함되어 있다. 있어야만 한다.

하지만 어플리케이션이 성장함네 따라 동일한 패키지 내에서 코드는 여러 조각으로 분할되었으므로 모든 조각을 최상위 디렉터리에 보관해야 할까? 이제 어플리케이션이 더 복잡해졌으니, 일을 좀 더 깔끔하게 정리할 때가 되었다.

## 설치가능한 단일 패키지
`helloworld.py`이 여전히 실행할 기본 스크립트이지만 모든 helper 메서드를 `helpers.py`이라는 새 파일로 이동했다고 가정해 보겠다.

`hellowold` Python 파일을 함께 패키징하겠지만 `README`, `.gitignore` 등과 같은 모든 기타 파일은 상위 디렉터리에 유지한다.

업데이트된 구조를 살펴보자.

```
helloworld/
│
├── helloworld/
│   ├── __init__.py
│   ├── helloworld.py
│   └── helpers.py
│
├── tests/
│   ├── helloworld_tests.py
│   └── helpers_tests.py
│
├── .gitignore
├── LICENSE
├── README.md
├── requirements.txt
└── setup.py
```

여기서 유일한 차이는 어플리케이션 코드가 모두 `helloworld` 하위 디렉토리(이 디렉토리는 패키지의 이름을 따서 명명됨)에 유지되고 `__init__py`라는 파일이 추가되었다는 것이다. 새로운 파일을 소개한다.

- `helloworld/__init__.py`: 이 파일은 많은 기능을 가지고 있지만, 여기에서는 이 디렉토리는 패키지 디렉토리라고 Python 인터프리터에 알려준다. 내부 모듈 구조를 알고`helloworld.helloworld` 또는 `helloworld.helpers`에서 import하는 대신 패키지 전체로 부터 클래스와 메서드를 가져올 수 있도록 이 `__init__.py` 파일을 설정할 수 있다. 

> **Note**: 내부 패키지와 `__init__.py`에 대한 자세한 내용은 [Python 모듈 및 패키지 개요](https://realpython.com/python-modules-packages/)를 참조하시오.

- `helloworld/helpers.py`: 위에서 언급한 바와 같이 `helloworld.py`의 비즈니스 로직을 대부분 이 파일로 옮겼다. `__init__.py` 덕분에 외부 모듈은 `helloworld` 패키지에서 가져오기만 하면 이러한 helpers를 액세스할 수 있다.

- `tests/`: 테스트를 자체 디렉터리로 이동했다. 프로그램 구조가 복잡해짐에 따라 이 패턴이 계속 나타날 것이다. 또한 패키지 구조를 반영하여 테스트를 별도의 모듈로 분할했다.

이 레이아웃은 [Kenneth Reitz’s samplemod application structure](https://github.com/kennethreitz/samplemod)에서 스트립된 버전이다. 이는 CLI 애플리케이션, 특히 더 광범위한 프로젝트의 또 다른 훌륭한 출발점이라 할 수 있다.

## 내부 패키지가 있는 어플리케이션
큰 어플리케이션에서는 주 실행 스크립트로 함께 연결되거나 패키징 중인 큰 라이브러리에 특정 기능을 제공하는 하나 이상의 내부 패키지가 있을 수 있다. 이것을 수용하기 위해 위에 제시된 규약을 확장할 것이다.

```
helloworld/
│
├── bin/
│
├── docs/
│   ├── hello.md
│   └── world.md
│
├── helloworld/
│   ├── __init__.py
│   ├── runner.py
│   ├── hello/
│   │   ├── __init__.py
│   │   ├── hello.py
│   │   └── helpers.py
│   │
│   └── world/
│       ├── __init__.py
│       ├── helpers.py
│       └── world.py
│
├── data/
│   ├── input.csv
│   └── output.xlsx
│
├── tests/
│   ├── hello
│   │   ├── helpers_tests.py
│   │   └── hello_tests.py
│   │
│   └── world/
│       ├── helpers_tests.py
│       └── world_tests.py
│
├── .gitignore
├── LICENSE
└── README.md
```

여기서 설명할 내용이 조금 더 있지만, 이전 레이아웃에서 설명한 것을 기억하는 한 이를 따라가는 것이 편할 것이다. 추가 및 수정 사항, 사용 용도, 필요한 이유 순으로 살펴보겠다.

- `bin/`: 이 디렉터리에는 모든 실행 파일들이 있다. 이는 [Jean-Paul Calderone’s classic structure post](http://as.ynchrono.us/2007/12/filesystem-structure-of-python-project_21.html)를 차용했고, 그의 `bin/` 디렉토리 사용에 대한 방법은 여전히 중요하다. 실행 파일에 많은 코드가 있어서는 안된다는 것이 기억해야 할 가장 중요한 점이며, 실행 스크립트의 [main function](https://realpython.com/python-main-function/)]에 대한 import와 호출만 있으면 된다는 것이다. 순수 Python을 사용 중이거나 실행 파일이 없는 경우 이 디렉토리를 생략할 수 있습다.
- `/docs`: 고급 응용 프로그램을 사용하면 모든 부분에 대한 문서를 잘 유지해야 한다. 내부 모듈에 대한 모든 문서를 여기에서 유지와 관리하려고 한다. 따라서 `hello` 패키지와 `world` 패키지에 대한 별도의 문서를 볼 수 있다. 내부 모듈에서 [docstrings](https://realpython.com/documenting-python-code/#documenting-your-python-code-base-using-docstrings)을 사용하는 경우(그래야 한다), 전체 모듈 설명서에서 최소한 모듈의 목적과 기능을 전체적으로 볼 수 있어야 한다.
- `helloworld/`: 이것은 이전 `helloworld/` 구조와 비슷하지만, 여기에는 하위 디렉토리가 있다. 복잡성이 증가할수록 "divide and conquer" 전략을 사용하여 어플리케이션 로직의 일부를 보다 관리하기 쉬운 청크로 분할해야 한다. 디렉터리 이름은 전체 패키지 이름을 나타내므로 하위 디렉터리 이름(`hello/`와 `world/`)에 패키지 이름을 반영해야 한다.
- `data/`: 이 디렉토리가 있으면 테스트에 도움을 준다. 이는 어플리케이션에서 수집하거나 생성하는 모든 파일의 중앙에 위치하고 있다. 어플리케이션을 배포하는 방법에 따라 "production-level" 입력 및 출력을 이 디렉터리를 가리키도록 유지하거나 내부 테스트에만 사용할 수 있다.
- `tests/`: 여 디렉토리에 모든 테스트(단위 테스트, 실행 테스트, 통합 테스트 등)를 넣을 수 있다. 테스트 전략, 가져오기 전략 등에 가장 편리한 방법으로 이 디렉터리를 구성한다. [4 Techniques for Testing Python Command-Line (CLI) Apps](https://realpython.com/python-cli-testing/)에서 Python을 사용하여 command-line 어플리케이션을 테스트하는 방법에 대한 최신 정보를 얻을 수 있다.

최상위 디렉토리의 파일은 이전 레이아웃과 거의 동일하다. command-line 어플리케이션 및 사용하는 GUI 프레임워크, 예를 들어 `tkinter`,에 따라 바뀌는 알림 수정 사항들이 있는 GUI 어플리케이션에서도 이 세 가지 레이아웃을 대부분의 사례에 적용할 수 있어야 한다.

> **Note**: 이는 단지 레이아웃일 뿐이다. 디렉터리나 파일이 특정 사례(예: 테스트를 배포하지 않는 경우 `test/`)에 맞지 않는 경우, 얼마든지 뺄 수 있다. 그러나 `docs/`는 빼지 않는다. 작업을 문서화하는 것은 항상 바람직한 것이다.
