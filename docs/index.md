# Python Application Layouts: A Reference

이는 [Python Application Layouts: A Reference](https://realpython.com/python-application-layouts/)를 편역한 것임.

## 목차

- [Commnand-Line 어플리케이션 레이아웃](cli-app.md)
- [Web 어플리케이션 레이아웃](web-app.md)
- [마치며](remarks.md)

Python은 구문과 스타일에 대해 의견이 많았지만 애플리케이션 구조화에 있어서는 놀라울 정도로 유연하다.

한편, 이러한 유연성은 매우 훌륭하다. 다양한 사용 사례에서 적합한 구조를 사용할 수 있다. 그러나 다른 한편으로, 이는 새로운 개발자에게 매우 혼란스러울 수 있다.

인터넷 역시 큰 도움이 되지 않는다. Python 블로그만큼 많은 의견들이 있다. 여기에서는 대부분의 사용 사례에 대해 참조할 수 있는 신뢰할 수 있는 **Python application layout reference guide**를 설명한다.

[명령줄 어플리케이션(CLI 어플리케이션)](https://realpython.com/python-command-line-arguments/), 일회성 스크립트, 설치 가능한 패키지, [Flask](https://realpython.com/tutorials/flask/)와 [Django](https://realpython.com/tutorials/django/)와 같은 대중적인 프레임워크가 포함된 웹 어플리케이션 레이아웃 등 일반적인 Python 애플리케이션 구조의 예를 보여 수 있다.

> **Notes**: 이 안내서는 Python 모듈 및 패키지에 대한 실무 지식이 필요한다. [Python 모듈 및 패키지](https://realpython.com/python-modules-packages/)에 대한 경험이 약간 오래되었다면 이에 대하여 새로 리프레시할 필요가 있다.
