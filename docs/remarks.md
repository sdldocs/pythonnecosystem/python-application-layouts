
이제 일회성 Python 스크립트, 설치 가능한 단일 패키지, 내부 패키지가 포함된 대규모 어플리케이션, Django 웹 어플리케이션, Flask 웹 어플리케이션 등 다양한 어플리케이션 유형에 대한 레이아웃 예를 살펴보았다.

이 범주에서 벗어나면 어디서부터 시작해야 할지 고민하며 빈 캔버스를 응시하지 않고 어플리케이션 구조를 구축하여 개발 중단을 성공적으로 방지할 수 있는 도구를 제공ㅘ고자 한다.

Python 어플리케이션 레이아웃에 대해서는 의견이 분분하므로, 실제에서는 사용 사례에 더 적합하도록 예제 레이아웃을 마음껏 변경하여 사용할 수 있다.

어플리케이션 레이아웃 참조뿐만 아니라 이러한 예제는 엄격한 규칙도 아니고 어플리케이션을 구조화하는 유일한 방법도 아니라는 점을 이해하길 바란다. 시간이 지남에 따라 연습을 통해 유용한 Python 어플리케이션 레이아웃을 직접 구축하고 커스톰화할 수 있는 능력을 개발하여야 할 것이다.
