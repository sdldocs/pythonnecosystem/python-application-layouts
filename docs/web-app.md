
Python의 주요 사용 사례중 하나는 [웹 어플리케이션](https://realpython.com/python-web-applications/)이다. [Django](https://www.djangoproject.com/)와 [Flask](http://flask.pocoo.org/)는 Python에서 가장 인기 있는 웹 프레임워크이며, 고맙게도 어플리케이션 레이아웃에 관해서는 조금 더 의견이 분분하다.

이 장에서 완전하고 본격적인 레이아웃 참조인지를 확인하기 위해 이러한 프레임워크에 공통되는 구조를 강조하고자 한다.

## Django
알파벳 순서대로 장고부터 시작합시다. Django의 좋은 점 중 하나는 `django-admin startproject project`를 실행한 후 프로젝트 골격을 생성한다는 것이다. 여기서 `project`는 프로젝트의 이름이다. 이렇게 하면 다음과 같은 내부 구조를 가진 `project`라는 현재 워킹 디렉터리 하위에 디렉터리가 생성된다.

```
project/
│
├── project/
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
│
└── manage.py
```

이건 좀 빈 것 같지 않아요? 모든 프로그램 로직은 어디에 있나? 뷰는? 심지어 어떤 테스트도 없네!

Django에서, 이것은 다른 Django 컨셉의 앱들을 묶는 프로젝트이다. 앱은 논리, 모델, 뷰 등이 모두 존재하는 곳이며, 그렇게 함으로써 블로그 유지와 같은 몇 가지 작업을 수행한다.

Django 앱을 프로젝트로 import하여 프로젝트 전체에서 사용할 수 있으며 특수 Python 패키지처럼 구조화되어 있다.

프로젝트와 마찬가지로, Django는 Django 앱 레이아웃을 매우 쉽게 생성할 수 있다. 프로젝트를 설정한 후 `manage.py`의 위치로 이동하여 `run python manage.py startapp app`을 한다. 여기서 `app`은 앱 이름이다.

이렇게 하면 다음과 같은 레이아웃을 가진 앱이라는 디렉터리가 생성된다.

```
app/
│
├── migrations/
│   └── __init__.py
│
├── __init__.py
├── admin.py
├── apps.py
├── models.py
├── tests.py
└── views.py
```

그런 다음 프로젝트로 직접 이를 import할 수 있다. 이러한 파일의 기능, 프로젝트에 사용하는 방법 등에 대한 자세한 내용은 이 튜토리얼의 범위를 벗어나지만 [Django tutorial](https://realpython.com/learn/start-django/)과 [offcial Django docs](https://docs.djangoproject.com/en/2.0/intro/tutorial01/)에서 이러한 모든 정보를 얻을 수 있다.

이 파일 및 폴더 구조는 매우 단순하며 장고의 기본 요구 사항이다. 모든 오픈 소스 Django 프로젝트의 경우 command-line 어플리케이션 레이아웃에서 구조를 준용할 수 있다. 일반적으로 바깥쪽 `project/` 디렉토리에 다음과 같이 내용이 표시된다.

```
project/
│
├── app/
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   │
│   ├── migrations/
│   │   └── __init__.py
│   │
│   ├── models.py
│   ├── tests.py
│   └── views.py
│
├── docs/
│
├── project/
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
│
├── static/
│   └── style.css
│
├── templates/
│   └── base.html
│
├── .gitignore
├── manage.py
├── LICENSE
└── README.md
```

고급 Django 어플리케이션 레이아웃에 대한 자세한 내용을 [this Stack Overflow thread](https://stackoverflow.com/questions/22841764/best-practice-for-django-project-working-directory-structure)에서 설명하고 있다. [django-project-skeleton]() 프로젝트 문서에서는 Stack Overflow thread에서 찾을 수 있는 디렉토리를 설명하고 있다. [Two Scoops of Django](https://realpython.com/asins/0692915729) 페이지에서 Django에 대한 포괄적인 정보를 찾을 수 있으며, Django 개발을 위한 모든 최신 모범 사례를 배울 수 있다.

[Django section at Real Python](https://realpython.com/tutorials/django/) 페이지를 방문하여 Django 튜토리얼에 대하여 보다 깊게 공부하십시오.

## Flask
Flask는 Python 웹 "마이크로 프레임워크"이다. 주요 세일즈 포인트 중 하나는 최소한의 오버헤드로 매우 빠르게 설정할 수 있다는 것이다. [Flask documentation](http://flask.pocoo.org/docs/1.0/)에는 10줄 이하의 코드와 단일 스크립트로 구성된 웹 어플리케이션(물론 실제로 이렇게 작은 웹 어플리케이션을 작성할 가능성은 거의 없지만) 예가 있다. 

다행히도 Flask ans서는 튜토리얼 프로젝트(Flaskr이라고 하는 블로그 웹 애플리케이션)에 대한 권장 레이아웃과 함께 [사용자를 구해주기 위해 달려들었고](http://flask.pocoo.org/docs/1.0/tutorial/layout/), 여기에서 main 프로젝트 디렉토리 내에서 부터 그 내용을 살펴볼 것이다.

```
flaskr/
│
├── flaskr/
│   ├── __init__.py
│   ├── db.py
│   ├── schema.sql
│   ├── auth.py
│   ├── blog.py
│   ├── templates/
│   │   ├── base.html
│   │   ├── auth/
│   │   │   ├── login.html
│   │   │   └── register.html
│   │   │
│   │   └── blog/
│   │       ├── create.html
│   │       ├── index.html
│   │       └── update.html
│   │ 
│   └── static/
│       └── style.css
│
├── tests/
│   ├── conftest.py
│   ├── data.sql
│   ├── test_factory.py
│   ├── test_db.py
│   ├── test_auth.py
│   └── test_blog.py
│
├── venv/
│
├── .gitignore
├── setup.py
└── MANIFEST.in
```

이러한 콘텐츠에서 대부분의 Python 어플리케이션과 마찬가지로 Flask 어플리케이션이 Python 패키지를 기반으로 구축되었음을 알 수 있다.

> **Note**: 아직 안 보이나요? 패키지를 찾기 위한 빠른 팁은 `__init__.py` 파일을 찾는 것이다. 이 파일은 해당 패키지의 최상위 디렉터리에 있다. 위의 레이아웃에서 `flaskr`은 `db`, `auth` 및 `blog` 모듈을 포함하는 패키지이다.

이 레이아웃에서는 [테스트](https://sdldocs.gitlab.io/pythonnecosystem/pytest/), [가상 환경](https://sdldocs.gitlab.io/pythonnecosystem/python-virtual-environments/) 디렉토리 및 일반적인 최상위 파일을 제외한 모든 항목이 flaskr 패키지에 저장된다. 다른 레이아웃과 마찬가지로, 테스트는 flaskr 패키지에 있는 개별 모듈과 대략 일치한다. 또한 템플릿은 main 프로젝트 패키지에 상주하며, 이는 Django 레이아웃에서는 볼 수 없다.

[Flask Boilerplate Github page](https://github.com/realpython/flask-boilerplate)를 방문하여 보다 완전한 기능을 갖춘 Flask 어플리케이션의 예를 살펴보고 [여기](http://www.flaskboilerplate.com/)에서 작동 중인 boilerplate를 보세요.

[플라스크 튜토리얼](https://realpython.com/tutorials/flask/)에서 Flask에 대하여 보다 자세히 살펴보세요.
